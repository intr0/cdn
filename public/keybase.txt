==================================================================
https://keybase.io/intr0_intr0
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://iosprivacy.com
  * I am intr0_intr0 (https://keybase.io/intr0_intr0) on keybase.
  * I have a public key ASAJUfbfq20Dz14_Tw7rQusT2u9xKFrfX8JHpvOY_selvgo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120ecf12960821d6e0a6ff659531689f3fdb4fd92241f511bf971398e1ccb24bbc70a",
      "host": "keybase.io",
      "kid": "01200951f6dfab6d03cf5e3f4f0eeb42eb13daef71285adf5fc247a6f398fec7a5be0a",
      "uid": "277f01ff58ce9adc3bd7416abb564f19",
      "username": "intr0_intr0"
    },
    "merkle_root": {
      "ctime": 1578496831,
      "hash": "935b649e203f00ed10b2b2b8688de2f7ee639d2a58394b1ab872dfd05e3c4cbc7ec77771e981e4c4f263d81095378728c20341e372caed12c24a7ddc34a203cc",
      "hash_meta": "6207a27aaf4423eda1eb743f10aac6a9c673b4766c0bdbf4d7f6bd095a960ae6",
      "seqno": 14139048
    },
    "service": {
      "entropy": "YN719Pb6kzWyvJtErPgqoDGS",
      "hostname": "iosprivacy.com",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.1.0"
  },
  "ctime": 1578496846,
  "expire_in": 504576000,
  "prev": "795477d7aea1a7532c3e3867b8ad493333508448e35ebc3f202d2eb541593b87",
  "seqno": 66,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgCVH236ttA89eP08O60LrE9rvcSha31/CR6bzmP7Hpb4Kp3BheWxvYWTESpcCQsQgeVR3166hp1MsPjhnuK1JMzNQhEjjXrw/IC0utUFZO4fEIIVZQeRp3vXgAc5v2MNcXTpKkyRoT6Hd/p/Q5FEQt7vfAgHCo3NpZ8RAXwBSOJy3cKjwsci6hoom3fe6bd5LcnNptx2FrdybOarV/53cMxnzRzHfaLG2lF+msuG7bljH7jtwbtDZqzmOBahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIMfiLJHn0HpqMVsXhYJm2aNDXa1s77HlluQESKKAXm2Eo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/intr0_intr0

==================================================================